<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 09/05/2018
 * Time: 23:27
 */

//Initiate sessions
$templatesArray=array('labels'=>array(),"volume"=>array(),'scheduling'=>array(),'environment'=>array(),'capabilities'=>array(),'request'=>array(),'scale'=>array(),'expose'=>array(),'ports'=>array(),'image'=>array(),'cmd'=>array());
session_start();
if(!$_SESSION["template"]) {
    $_SESSION["template"] = $templatesArray;
}
if(!$_SESSION["serviceTemplates"]) {
    $_SESSION["serviceTemplates"] =array();
}


if($_POST["how"]==="save"){
    templateToSession();
}
if($_POST["how"]==="get"){
    getTemplateFromSession();
}
if($_GET["how"]==="saveService"){
    serviceTemplateToSession();
}
if($_POST["how"]==="getService"){
    getServiceTemplateNameFromSession();
}

/**
 * Get specific parameter template from session according to the template class
 */
function getTemplateFromSession(){
    $classData=array();
    $templateClass= $_POST["templateClass"];
    session_start();
    $templatesArray = $_SESSION["template"];
    foreach ($templatesArray as $key => $value) {
        if ($key === $templateClass) {
          $classData=$templatesArray[$key];
        }
    }
   $json=json_encode($classData);
    echo $json;

}

/**
 * Get all names of services' templates from session
 */
function getServiceTemplateNameFromSession(){
     session_start();
    $json=json_encode($_SESSION["serviceTemplates"]);
    echo $json;
}

/**
 * Save parameter template to session
 */
function templateToSession(){
    session_start();
    $templatesArray = $_SESSION["template"];
    $templateClass = $_POST["templateClass"];
    $dataArray = $_POST ["dataArray"];

    foreach ($templatesArray as $key => $value) {
        if ($key === $templateClass) {
            $length = count($templatesArray[$key]);
            $templatesArray[$key][$length] = $dataArray;
        }
    }
    $_SESSION["template"] = $templatesArray;
   echo "Save Successfully!";
}

/**
 * Save service template to session
 */
function serviceTemplateToSession(){
    $info = $_POST;
    $json=json_encode($info,JSON_PRETTY_PRINT);
    $obj=json_decode($json,true);
    session_start();
    $templateName=$_GET["templateName"];
    $serviceIndex=$_GET["serviceIndex"];
    $serviceTemplates=$_SESSION["serviceTemplates"];
    $serviceTemplates[$templateName]=$obj["services"][$serviceIndex];
    $_SESSION["serviceTemplates"]=$serviceTemplates;
    echo "Save successfully!";
}


